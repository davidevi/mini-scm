terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "s3" {
    # Configured during `terraform init` so that it can be parameterised
    # See the `deploy` task for more information
    key = "terraform.tfstate"
  }

}

provider "aws" {}

variable "ENVIRONMENT_NAME" {
  description = "The name of the environment"
  type        = string
}

variable "OPENAI_API_KEY" {
  description = "The Open AI API key"
  type        = string
}

variable "EMAIL_DOMAIN" {
  description = "Domain used for receiving emails"
  type        = string
}

variable "SLACK_WEBHOOK" {
  description = "Slack webhook URL"
  type        = string
}

variable "GRAFANA_AWS_USER_ID" {
  description = "Grafana user"
  type        = string
}

module "aws" {
  source              = "./aws"
  ENVIRONMENT_NAME    = var.ENVIRONMENT_NAME
  OPENAI_API_KEY      = var.OPENAI_API_KEY
  EMAIL_DOMAIN        = var.EMAIL_DOMAIN
  SLACK_WEBHOOK       = var.SLACK_WEBHOOK
  GRAFANA_AWS_USER_ID = var.GRAFANA_AWS_USER_ID
}
