
resource "aws_ses_receipt_rule_set" "receipt_rules" {
  rule_set_name = "miniscm-${var.ENVIRONMENT_NAME}-receipt-rules"

}

resource "aws_ses_receipt_rule" "upload_emails_to_s3" {
  rule_set_name = aws_ses_receipt_rule_set.receipt_rules.rule_set_name
  name          = "miniscm-${var.ENVIRONMENT_NAME}-upload-to-receipts-bucket"
  enabled       = true
  scan_enabled  = false
  recipients = [
    "receipts-${var.ENVIRONMENT_NAME}@${var.EMAIL_DOMAIN}"
  ]

  s3_action {
    bucket_name       = aws_s3_bucket.receipts_bucket.bucket
    object_key_prefix = "emails/"
    position          = 1
  }

  depends_on = [
    aws_s3_bucket.receipts_bucket
  ]

}

data "aws_caller_identity" "current" {}

resource "aws_s3_bucket_policy" "allow_ses_to_write_to_bucket" {
  bucket = aws_s3_bucket.receipts_bucket.bucket

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ses.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.receipts_bucket.bucket}/*",
      "Condition": {
        "StringEquals": {
          "AWS:SourceAccount": "${data.aws_caller_identity.current.account_id}"
        }
      }
    }
  ]
}  
EOF
}

resource "aws_ses_active_receipt_rule_set" "activate_receipt_rules" {
  rule_set_name = aws_ses_receipt_rule_set.receipt_rules.rule_set_name
}
