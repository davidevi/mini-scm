
output "grafana_url" {
  value = aws_grafana_workspace.grafana_workspace.endpoint
}

output "grafana_api_key" {
  value     = aws_grafana_workspace_api_key.key.key
  sensitive = true
}
