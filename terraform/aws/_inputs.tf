
variable "ENVIRONMENT_NAME" {
  description = "The name of the environment"
  type        = string
}

variable "OPENAI_API_KEY" {
  description = "The Open AI API key"
  type        = string
}

variable "EMAIL_DOMAIN" {
  description = "Domain used for receiving emails"
  type        = string
}

variable "SLACK_WEBHOOK" {
  description = "Slack webhook URL"
  type        = string
}

variable "GRAFANA_AWS_USER_ID" {
  description = "Grafana user"
  type        = string
}
