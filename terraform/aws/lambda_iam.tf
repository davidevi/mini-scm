
data "aws_iam_policy_document" "lambda_assume_role_policy_doc" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "logging_policy_doc" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:aws:logs:*:*:*"]
  }
}

data "aws_iam_policy_document" "receipts_bucket_rw" {
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:ListObjects",
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.receipts_bucket.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.receipts_bucket.bucket}/*",
    ]
  }
}

data "aws_iam_policy_document" "textract_start_document_text_detection" {
  statement {
    effect = "Allow"

    actions = [
      "textract:StartDocumentTextDetection",
      "textract:GetDocumentTextDetection"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "lambda_logging_policy" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"
  policy      = data.aws_iam_policy_document.logging_policy_doc.json
}

resource "aws_iam_policy" "receipts_bucket_rw" {
  name        = "receipts_bucket_rw"
  path        = "/"
  description = "IAM policy for reading and writing to the receipts bucket"
  policy      = data.aws_iam_policy_document.receipts_bucket_rw.json
}

resource "aws_iam_policy" "textract_start_document_text_detection" {
  name        = "textract_start_document_text_detection"
  path        = "/"
  description = "IAM policy for starting a textract document text detection job"
  policy      = data.aws_iam_policy_document.textract_start_document_text_detection.json
}

resource "aws_iam_role" "mini_scm_lambda_role" {
  name               = "miniscm-${var.ENVIRONMENT_NAME}_mini_scm_lambdas_role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy_doc.json
}

resource "aws_iam_role_policy_attachment" "lambda_logging_policy_attachment" {
  role       = aws_iam_role.mini_scm_lambda_role.name
  policy_arn = aws_iam_policy.lambda_logging_policy.arn
}

resource "aws_iam_role_policy_attachment" "receipts_bucket_rw_policy_attachment" {
  role       = aws_iam_role.mini_scm_lambda_role.name
  policy_arn = aws_iam_policy.receipts_bucket_rw.arn
}

resource "aws_iam_role_policy_attachment" "textract_start_document_text_detection_policy_attachment" {
  role       = aws_iam_role.mini_scm_lambda_role.name
  policy_arn = aws_iam_policy.textract_start_document_text_detection.arn
}
