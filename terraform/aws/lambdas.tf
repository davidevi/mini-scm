
#region MiniSCM Dependencies

data "archive_file" "mini_scm_dependencies" {
  type        = "zip"
  source_dir  = "${path.root}/../lambdas/mini_scm_dependencies"
  output_path = "${path.module}/lambdas/mini_scm_dependencies.zip"
}

resource "aws_lambda_layer_version" "mini_scm_dependencies" {
  filename            = "${path.module}/lambdas/mini_scm_dependencies.zip"
  source_code_hash    = data.archive_file.mini_scm_dependencies.output_base64sha256
  layer_name          = "mini_scm_dependencies"
  compatible_runtimes = ["python3.12"]
  depends_on          = [data.archive_file.mini_scm_dependencies]
}

#endregion

#region MiniSCM Common

data "archive_file" "mini_scm_common" {
  type        = "zip"
  source_dir  = "${path.root}/../lambdas/mini_scm_common"
  output_path = "${path.module}/lambdas/mini_scm_common.zip"
}

resource "aws_lambda_layer_version" "mini_scm_layer" {
  filename            = "${path.module}/lambdas/mini_scm_common.zip"
  source_code_hash    = data.archive_file.mini_scm_common.output_base64sha256
  layer_name          = "mini_scm_common"
  compatible_runtimes = ["python3.12"]
}

#endregion

#region Process Email

data "archive_file" "process_email" {
  type        = "zip"
  source_dir  = "${path.root}/../lambdas/process_email"
  output_path = "${path.module}/lambdas/process_email.zip"
}

resource "aws_lambda_function" "process_email" {
  filename         = "${path.module}/lambdas/process_email.zip"
  source_code_hash = data.archive_file.process_email.output_base64sha256
  function_name    = "miniscm-${var.ENVIRONMENT_NAME}_process_email"
  handler          = "main.lambda_handler"
  runtime          = "python3.12"
  role             = aws_iam_role.mini_scm_lambda_role.arn
  layers = [
    aws_lambda_layer_version.mini_scm_dependencies.arn,
    aws_lambda_layer_version.mini_scm_layer.arn
  ]
  timeout = 20

  environment {
    variables = {
      "SLACK_WEBHOOK" = var.SLACK_WEBHOOK
    }
  }
  depends_on = [aws_lambda_layer_version.mini_scm_dependencies]
}

resource "aws_lambda_function_event_invoke_config" "process_email_config" {
  function_name          = aws_lambda_function.process_email.function_name
  maximum_retry_attempts = 0
}

#endregion

#region OCR

data "archive_file" "ocr" {
  type        = "zip"
  source_dir  = "${path.root}/../lambdas/ocr"
  output_path = "${path.module}/lambdas/ocr.zip"
}

resource "aws_lambda_function" "ocr" {
  filename         = "${path.module}/lambdas/ocr.zip"
  source_code_hash = data.archive_file.ocr.output_base64sha256
  function_name    = "miniscm-${var.ENVIRONMENT_NAME}_ocr"
  handler          = "main.lambda_handler"
  runtime          = "python3.12"
  role             = aws_iam_role.mini_scm_lambda_role.arn
  layers = [
    aws_lambda_layer_version.mini_scm_dependencies.arn,
    aws_lambda_layer_version.mini_scm_layer.arn
  ]
  timeout = 300

  environment {
    variables = {
      "SLACK_WEBHOOK" = var.SLACK_WEBHOOK
    }
  }
}

resource "aws_lambda_function_event_invoke_config" "ocr_config" {
  function_name          = aws_lambda_function.ocr.function_name
  maximum_retry_attempts = 0
}

#endregion

#region Stitch

data "archive_file" "stitch" {
  type        = "zip"
  source_dir  = "${path.root}/../lambdas/stitch"
  output_path = "${path.module}/lambdas/stitch.zip"
}

resource "aws_lambda_function" "stitch" {
  filename         = "${path.module}/lambdas/stitch.zip"
  source_code_hash = data.archive_file.stitch.output_base64sha256
  function_name    = "miniscm-${var.ENVIRONMENT_NAME}_stitch"
  handler          = "main.lambda_handler"
  runtime          = "python3.12"
  role             = aws_iam_role.mini_scm_lambda_role.arn
  layers = [
    aws_lambda_layer_version.mini_scm_dependencies.arn,
    aws_lambda_layer_version.mini_scm_layer.arn
  ]
  timeout = 20

  environment {
    variables = {
      "SLACK_WEBHOOK" = var.SLACK_WEBHOOK
    }
  }
}

resource "aws_lambda_function_event_invoke_config" "stitch_config" {
  function_name          = aws_lambda_function.stitch.function_name
  maximum_retry_attempts = 0
}

#endregion

#region Structure

data "archive_file" "structure" {
  type        = "zip"
  source_dir  = "${path.root}/../lambdas/structure"
  output_path = "${path.module}/lambdas/structure.zip"
}

resource "aws_lambda_function" "structure" {
  filename         = "${path.module}/lambdas/structure.zip"
  source_code_hash = data.archive_file.structure.output_base64sha256
  function_name    = "miniscm-${var.ENVIRONMENT_NAME}_structure"
  handler          = "main.lambda_handler"
  runtime          = "python3.12"
  role             = aws_iam_role.mini_scm_lambda_role.arn
  layers = [
    aws_lambda_layer_version.mini_scm_dependencies.arn,
    aws_lambda_layer_version.mini_scm_layer.arn
  ]
  timeout = 300

  environment {
    variables = {
      "OPENAI_API_KEY" = var.OPENAI_API_KEY
      "SLACK_WEBHOOK"  = var.SLACK_WEBHOOK
    }
  }
}

resource "aws_lambda_function_event_invoke_config" "structure_config" {
  function_name          = aws_lambda_function.structure.function_name
  maximum_retry_attempts = 0
}


#endregion
