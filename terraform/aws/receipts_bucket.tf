#region The Bucket

resource "aws_s3_bucket" "receipts_bucket" {
  bucket = "miniscm-${var.ENVIRONMENT_NAME}-receipts"
}

#endregion

#region Lambda Triggers, configured from the bucket side 

resource "aws_s3_bucket_notification" "receipt_bucket_lambda_triggers" {
  bucket = aws_s3_bucket.receipts_bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.process_email.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "emails"
    # Suffix is not actually added by SES when saving
    # filter_suffix       = ".eml"
  }

  lambda_function {
    lambda_function_arn = aws_lambda_function.ocr.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "scans"
    filter_suffix       = ".jpeg"
  }

  lambda_function {
    lambda_function_arn = aws_lambda_function.stitch.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "ocr"
    filter_suffix       = "1.json"
  }

  lambda_function {
    lambda_function_arn = aws_lambda_function.structure.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "text"
    filter_suffix       = ".txt"
  }

  depends_on = [
    aws_lambda_function.process_email,
    aws_lambda_function.ocr,
    aws_lambda_function.stitch,
    aws_lambda_function.structure,
  ]
}

#endregion

#region Permissions for the bucket to trigger the lambdas

resource "aws_lambda_permission" "allow_bucket_call_process_email" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.receipts_bucket.arn
  function_name = aws_lambda_function.process_email.function_name
}

resource "aws_lambda_permission" "allow_bucket_call_ocr" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.receipts_bucket.arn
  function_name = aws_lambda_function.ocr.function_name
}

resource "aws_lambda_permission" "allow_bucket_call_stitch" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.receipts_bucket.arn
  function_name = aws_lambda_function.stitch.function_name
}

resource "aws_lambda_permission" "allow_bucket_call_structure" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.receipts_bucket.arn
  function_name = aws_lambda_function.structure.function_name
}
#endregion
