
resource "aws_glue_catalog_database" "database" {
  name        = "miniscm-${var.ENVIRONMENT_NAME}"
  description = "Mini SCM Database for ${var.ENVIRONMENT_NAME}"
}

resource "aws_glue_registry" "schema_registry" {
  registry_name = "miniscm-${var.ENVIRONMENT_NAME}"
}

resource "aws_athena_workgroup" "workgroup" {
  name  = "miniscm-${var.ENVIRONMENT_NAME}"
  state = "ENABLED"
  configuration {
    enforce_workgroup_configuration = true
    result_configuration {
      output_location = "s3://${aws_s3_bucket.receipts_bucket.bucket}/athena-results/"
    }
  }

  tags = {
    // https://docs.aws.amazon.com/grafana/latest/userguide/Athena-prereq.html
    GrafanaDataSource = "true"
  }
}

# region Shops =================================================================

resource "aws_glue_catalog_table" "shops" {
  name          = "shops"
  database_name = aws_glue_catalog_database.database.name
  table_type    = "EXTERNAL_TABLE"

  parameters = {
    "classification" = "json"
  }

  storage_descriptor {
    location      = "s3://${aws_s3_bucket.receipts_bucket.bucket}/data/shops/"
    input_format  = "org.apache.hadoop.mapred.TextInputFormat"
    output_format = "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"

    ser_de_info {
      name                  = "org.openx.data.jsonserde.JsonSerDe"
      serialization_library = "org.openx.data.jsonserde.JsonSerDe"
      parameters = {
        "serialization.format" = "1"
      }
    }

    schema_reference {
      schema_id {
        schema_arn = aws_glue_schema.shops_schema.arn
      }
      schema_version_number = aws_glue_schema.shops_schema.latest_schema_version
    }
  }
}

resource "aws_glue_schema" "shops_schema" {
  schema_name       = "shops"
  registry_arn      = aws_glue_registry.schema_registry.arn
  data_format       = "JSON"
  compatibility     = "NONE"
  schema_definition = file("${path.module}/schemas/shops.schema.json")
}

resource "aws_athena_named_query" "shops_query" {
  name     = "[ MiniSCM ${var.ENVIRONMENT_NAME} ] List all shops"
  database = aws_glue_catalog_database.database.name
  query    = "SELECT * FROM shops"
}

# endregion ====================================================================


# region Purchases =============================================================

resource "aws_glue_catalog_table" "purchases" {
  name          = "purchases"
  database_name = aws_glue_catalog_database.database.name
  table_type    = "EXTERNAL_TABLE"
  parameters = {
    "classification" = "json"
  }

  storage_descriptor {
    location      = "s3://${aws_s3_bucket.receipts_bucket.bucket}/data/purchases/"
    input_format  = "org.apache.hadoop.mapred.TextInputFormat"
    output_format = "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"

    ser_de_info {
      name                  = "org.openx.data.jsonserde.JsonSerDe"
      serialization_library = "org.openx.data.jsonserde.JsonSerDe"
      parameters = {
        "serialization.format" = "1"
      }
    }

    schema_reference {
      schema_id {
        schema_arn = aws_glue_schema.purchases_schema.arn
      }
      schema_version_number = aws_glue_schema.purchases_schema.latest_schema_version
    }
  }
}

resource "aws_glue_schema" "purchases_schema" {
  schema_name       = "purchases"
  registry_arn      = aws_glue_registry.schema_registry.arn
  data_format       = "JSON"
  compatibility     = "NONE"
  schema_definition = file("${path.module}/schemas/purchases.schema.json")
}

resource "aws_athena_named_query" "purchases_query" {
  name     = "[ MiniSCM ${var.ENVIRONMENT_NAME} ] List all purchases"
  database = aws_glue_catalog_database.database.name
  query    = "SELECT * FROM purchases"
}

# endregion ====================================================================
