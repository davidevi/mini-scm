
#region IAM 

resource "aws_iam_role" "assume" {
  name = "grafana-assume"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "grafana.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_policy" "name" {
  name        = "grafana-athena-policy"
  description = "Policy to allow Grafana to use Athena"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "AthenaQueryAccess"
        Effect = "Allow"
        Action = [
          "athena:ListDatabases",
          "athena:ListDataCatalogs",
          "athena:ListWorkGroups",
          "athena:GetDatabase",
          "athena:GetDataCatalog",
          "athena:GetQueryExecution",
          "athena:GetQueryResults",
          "athena:GetTableMetadata",
          "athena:GetWorkGroup",
          "athena:ListTableMetadata",
          "athena:StartQueryExecution",
          "athena:StopQueryExecution"
        ]
        Resource = ["*"]
      },
      {
        Sid    = "GlueReadAccess"
        Effect = "Allow"
        Action = [
          "glue:GetDatabase",
          "glue:GetDatabases",
          "glue:GetTable",
          "glue:GetTables",
          "glue:GetPartition",
          "glue:GetPartitions",
          "glue:BatchGetPartition"
        ]
        Resource = ["*"]
      },
      {
        Sid    = "AthenaS3Access"
        Effect = "Allow"
        Action = [
          "s3:GetBucketLocation",
          "s3:GetObject",
          "s3:ListBucket",
          "s3:ListBucketMultipartUploads",
          "s3:ListMultipartUploadParts",
          "s3:AbortMultipartUpload",
          "s3:PutObject"
        ]
        Resource = ["arn:aws:s3:::${aws_s3_bucket.receipts_bucket.bucket}/athena-results/*"]
      },
    ]
  })
}

resource "aws_iam_policy_attachment" "name" {
  name       = "grafana-athena-policy-attachment"
  policy_arn = aws_iam_policy.name.arn
  roles      = [aws_iam_role.assume.name]
}

#endregion

data "aws_organizations_organization" "current" {}

resource "aws_grafana_workspace" "grafana_workspace" {
  account_access_type      = "ORGANIZATION"
  organizational_units     = [data.aws_organizations_organization.current.roots[0].id]
  authentication_providers = ["AWS_SSO"]
  permission_type          = "SERVICE_MANAGED"
  role_arn                 = aws_iam_role.assume.arn

  data_sources    = ["ATHENA"]
  name            = "miniscm-${var.ENVIRONMENT_NAME}"
  description     = "MiniSCM ${var.ENVIRONMENT_NAME} Grafana Workspace"
  grafana_version = 9.4
  configuration   = "{\"plugins\": {\"pluginAdminEnabled\": true }}"

  lifecycle {
    ignore_changes = [
      configuration,
    ]
  }
}

resource "aws_grafana_role_association" "grafana_user_association" {
  role         = "ADMIN"
  user_ids     = [var.GRAFANA_AWS_USER_ID]
  workspace_id = aws_grafana_workspace.grafana_workspace.id
}

resource "aws_grafana_workspace_api_key" "key" {
  key_name        = "terraform-key"
  key_role        = "ADMIN"
  seconds_to_live = 3600
  workspace_id    = aws_grafana_workspace.grafana_workspace.id
}

