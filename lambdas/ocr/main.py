"""
    Given a receipt image uploaded to S3, perform OCR using Textract 
    and save the reuslts to S3

    Trigger should be:
    - Is an upload
    - Is an image
    - In the "scans" directory
"""

import os
import sys
import time
import json
import logging

import boto3

from receipts import get_receipt_event_structure, PATH_OCR
from slack import send_slack_message


def lambda_handler(event, context):
    try:
        info = get_receipt_event_structure(event)
        send_slack_message(f"[ {info['receipt_id']} ] Performing OCR on part {info['receipt_part']}")

        logging.info(f"Performing OCR on {info['receipt_id']} part {info['receipt_part']}")

        ocr_result = perform_ocr_on_receipt_part(info["bucket"], event["Records"][0]["s3"]["object"]["key"])

        save_ocr_result(info["bucket"], info["receipt_id"], info["receipt_part"], ocr_result)

        send_slack_message(f"[ {info['receipt_id']} ] OCR complete for part {info['receipt_part']}")
        return json.dumps(ocr_result)
    except Exception as e:
        logging.error(f"Error processing receipt: {e}")
        send_slack_message(f"Error processing receipt: {e}")
        sys.exit(1)


def perform_ocr_on_receipt_part(bucket, key):
    textract = boto3.client("textract", region_name=os.environ["AWS_REGION"])

    response = textract.start_document_text_detection(DocumentLocation={"S3Object": {"Bucket": bucket, "Name": key}})

    job_id = response["JobId"]

    while True:
        job_status = textract.get_document_text_detection(JobId=job_id)
        status = job_status["JobStatus"]
        if status in ["SUCCEEDED", "FAILED"]:
            break
        time.sleep(5)

    if status == "SUCCEEDED":
        logging.info("OCR job succeeded")
        result = textract.get_document_text_detection(JobId=job_id)
        return result["Blocks"]

    logging.error(f"Job failed: {job_status['StatusMessage']}")
    sys.exit(1)


def save_ocr_result(bucket, receipt_id, receipt_part, text):
    s3 = boto3.client("s3")
    s3.put_object(
        Bucket=bucket,
        Key=os.path.join(PATH_OCR, receipt_id, f"{receipt_part}.json"),
        Body=json.dumps(text),
    )
    logging.info(f"Saved OCR results to s3://{bucket}/{PATH_OCR}/{receipt_id}/{receipt_part}.json")
