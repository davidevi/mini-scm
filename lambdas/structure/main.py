"""
    Given the full text of a receipt, produce a structured JSON object
    representing the data in the receipt.

    Trigger should be:
    - Is an upload
    - Is a text file
    - In the "text" directory
"""

import sys
import json
import logging

import boto3
from jsonschema import validate, ValidationError
from openai import OpenAI

from receipts import get_receipt_event_structure, PATH_TEXT, PATH_DATA
from slack import send_slack_message

S3 = boto3.client("s3")


def lambda_handler(event, context):
    try:

        info = get_receipt_event_structure(event)

        send_slack_message(f"[ {info['receipt_id']} ] Structuring data")

        full_text = get_receipt_text(info["bucket"], info["receipt_id"])
        logging.debug(f"Successfully retrieved full text for receipt {info['receipt_id']}")

        raw_structured_data = structure_data_from_receipt(full_text)

        if not validate_structured_data(raw_structured_data):
            sys.exit(1)

        structured_data = json.loads(raw_structured_data)
        if "date" not in structured_data:
            structured_data["date"] = info["receipt_id"]

        save_data(S3, info["bucket"], info["receipt_id"], f"structured/{info['receipt_id']}", raw_structured_data)

        shop_data = create_shop_from_data(structured_data)
        logging.info(f"Created shop data for receipt {info['receipt_id']}:\n{shop_data}")

        purchases_data = create_purchases_from_data(structured_data)
        logging.info(f"Created purchases data for receipt {info['receipt_id']}:\n{purchases_data}")

        save_data(S3, info["bucket"], info["receipt_id"], f"shops/{info['receipt_id']}", json.dumps(shop_data))
        stripped_purchases_data = ""
        for item in purchases_data:
            stripped_purchases_data += json.dumps(item) + "\n"
        save_data(S3, info["bucket"], info["receipt_id"], f"purchases/{info['receipt_id']}", stripped_purchases_data)

        send_slack_message(
            f"[ {info['receipt_id']} ] Structured data has been saved (found {len(purchases_data)} items)"
        )
        return structured_data

    except Exception as e:
        logging.error(f"Error structuring receipt: {e}")
        send_slack_message(f"Error structuring receipt: {e}")
        sys.exit(1)


def get_receipt_text(bucket, receipt_id):
    response = S3.get_object(Bucket=bucket, Key=f"{PATH_TEXT}/{receipt_id}/full_text.txt")
    return response["Body"].read().decode("utf-8")


def get_schema():
    with open("receipt.schema.json") as f:
        return f.read()


def structure_data_from_receipt(full_text: str) -> dict:
    """
    Given the full text of a receipt, produce a JSON object with the structured data.
    """
    client = OpenAI()
    logging.debug("Making request to OpenAI")
    response = client.chat.completions.create(
        model="gpt-4",
        messages=[
            {
                "role": "system",
                "content": "Given the OCR scanned text of a receipt, produce a JSON object with the structured data.",
            },
            {
                "role": "system",
                "content": f"The newly created JSON object should be based off this JSON schema: ```{get_schema()}```",
            },
            {
                "role": "system",
                "content": "(Don't just replace items in the schema with the values you find, you should create a brand new JSON that _follows_ the schema)",
            },
            {
                "role": "system",
                "content": "The scanned text might have overlapping text, make sure items are not duplicated in the JSON object",
            },
            {
                "role": "system",
                "content": "Do NOT truncate the text, use the full text of the receipt",
            },
            {
                "role": "system",
                "content": "Do NOT add fields that are not present in the schema",
            },
            {
                "role": "system",
                "content": "ALL items from the receipt should be included in the JSON object",
            },
            {
                "role": "system",
                "content": "Return a valid JSON object and no other text nor any markdown formatting",
            },
            {
                "role": "system",
                "content": "'weight' is mandatory. If it can't be found, set it to an empty string",
            },
            {
                "role": "system",
                "content": "Fields that are not required can be omitted",
            },
            {
                "role": "system",
                "content": "You can may take creative liberties with the categories, always use the example ones if they match but add more if required",
            },
            {
                "role": "system",
                "content": "Aim for each item to have a minimum of 1 category, but at least 5 is ideal",
            },
            {
                "role": "system",
                "content": "There is nothing more important that following the schema, make sure the JSON object matches it exactly",
            },
            {"role": "user", "content": full_text},
        ],
        temperature=0,
    )
    logging.debug("OK response from OpenAI")
    logging.debug(response.choices[0].message.content)
    return response.choices[0].message.content


def save_data(S3, bucket, receipt_id, path, stripped_data):
    S3.put_object(
        Bucket=bucket,
        Key=f"{PATH_DATA}/{path}.json",
        Body=stripped_data.encode("utf-8"),
    )
    logging.info(f"Saved structured data for receipt {receipt_id} at s3://{bucket}/{PATH_DATA}/{path}.json")


def validate_structured_data(raw_structured_data):
    logging.info("Validating structured data")
    try:
        # 1. Is actual JSON
        structured_data = json.loads(raw_structured_data)
        logging.info("[ OK ] Structured data is valid JSON")

        # 2. Schema matches
        with open("receipt.schema.json") as f:
            schema = json.load(f)
        validate(structured_data, schema)
        logging.info("[ OK ] Structured data matches schema")

        # 3. Subtotal matches
        actual_subtotal = 0
        for item in structured_data["items"]:
            actual_subtotal += item["price"]
            print(f"+ {item['price']} => {actual_subtotal}\t\t({item['description']})")

        ACCEPTABLE_ERROR = 5

        if abs(actual_subtotal - structured_data["subtotal"]) > ACCEPTABLE_ERROR:
            logging.error(f"Validation failed: expected subtotal {structured_data['subtotal']}, got {actual_subtotal}")
            return False

    except json.JSONDecodeError:
        logging.error("Validation failed: structured data is not valid JSON")
        send_slack_message("Validation failed: structured data is not valid JSON")
        return False

    except ValidationError as e:
        logging.error(f"Validation failed: structured data does not match schema: {e}")
        send_slack_message(f"Validation failed: structured data does not match schema: {e}")
        return False

    except TypeError:
        logging.error("Validation failed: structured data is does not have the expected structure")
        send_slack_message("Validation failed: structured data is does not have the expected structure")
        return False

    logging.info("Validation passed")
    return True


def create_shop_from_data(structured_data):
    """
    Given a receipt, create a record of this shopping trip;
    Excludes items but includes the total cost and a total of the number of items.
    """
    return {
        "chain": structured_data["chain"],
        "store": structured_data["store"],
        "subtotal": structured_data["subtotal"],
        "savings": structured_data["savings"] if "savings" in structured_data else 0,
        "promotions": structured_data["promotions"] if "promotions" in structured_data else 0,
        "total": structured_data["total"],
        "num_items": len(structured_data["items"]),
        "date": structured_data["date"],
    }


def create_purchases_from_data(structured_data):
    """
    Given a receipt, convert the items into a list of purchases,
    containing the original item information and the shop information.
    """
    purchases = []

    for item in structured_data["items"]:

        clean_weight = 0
        price_per_weight = 0
        if "weight" in item:
            # remove any non-numeric characters from the weight
            clean_weight = "".join([c for c in item["weight"] if c.isdigit() or c == "."])
            if clean_weight == "":
                clean_weight = 0
            try:
                price_per_weight = float(item["price"]) / float(clean_weight)
            except ZeroDivisionError:
                price_per_weight = item["price"]

        purchases.append(
            {
                "chain": structured_data["chain"],
                "store": structured_data["store"],
                "description": item["description"],
                "price": item["price"],
                "price_per_item": item["price"] / item["quantity"],
                "weight": clean_weight,
                "weight_unit": item["weight_unit"] if "weight_unit" in item else "",
                "price_per_weight": price_per_weight,
                "quantity": item["quantity"],
                "categories": item["categories"],
                "date": structured_data["date"],
            }
        )

    return purchases
