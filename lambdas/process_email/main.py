import sys
import boto3
import email
import logging
from datetime import datetime

from receipts import PATH_SCANS
from slack import send_slack_message


def lambda_handler(event, context):
    try:
        bucket_name = event["Records"][0]["s3"]["bucket"]["name"]
        email_key = event["Records"][0]["s3"]["object"]["key"]
        send_slack_message(f"Processing new email {email_key} in {bucket_name}")

        email = get_email_from_key(bucket_name, email_key)
        attachments = get_attachements_from_email(email)
        receipt_id = create_receipt_id(email)

        # Sort attachements by filename so that images are in the correct order
        attachments.sort(key=lambda x: x.get_filename())

        upload_receipt_parts(bucket_name, receipt_id, attachments)

        send_slack_message(f"[ {receipt_id} ] Processed email for receipt in {len(attachments)} parts")
        return [attachement.get_filename() for attachement in attachments]

    except Exception as e:
        logging.error(f"Error processing email: {e}")
        send_slack_message(f"Error processing email: {e}")
        sys.exit(1)


def get_email_from_key(bucket, key):
    """
    Loads the file from the S3 key and returns an email object
    """
    s3 = boto3.client("s3")
    obj = s3.get_object(Bucket=bucket, Key=key)
    email_data = obj["Body"].read()
    logging.info(f"Loaded email from {bucket}/{key}")
    return email.message_from_bytes(email_data)


def get_attachements_from_email(email):
    """
    Extracts the attachments from the email
    """
    attachments = []
    for part in email.walk():
        if part.get_content_maintype() == "multipart":
            continue
        if part.get("Content-Disposition") is None:
            continue
        attachments.append(part)
    logging.info(f"Found {len(attachments)} attachments")
    return attachments


def create_receipt_id(email):
    """
    Receipt ID is the "yyyy-mm-dd-hh-mm" of the email
    """
    email_date = datetime.strptime(email["Date"], "%a, %d %b %Y %H:%M:%S %z")
    receipt_id = email_date.strftime("%Y-%m-%d-%H-%M")
    logging.info(f"Created receipt ID: {receipt_id}")
    return receipt_id


def upload_receipt_parts(bucket, receipt_id, attachments):
    """
    Uploads the receipt parts to the bucket
    """
    s3 = boto3.client("s3")
    for i, attachment in enumerate(attachments):
        attachement_extension = attachment.get_filename().split(".")[-1]
        key = f"{PATH_SCANS}/{receipt_id}/{i + 1}.{attachement_extension}"
        s3.put_object(Bucket=bucket, Key=key, Body=attachment.get_payload(decode=True))
        logging.info(f"Uploaded {bucket}/{key}")
    logging.info(f"Uploaded {len(attachments)} parts")
