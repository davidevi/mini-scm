"""
    Contains constants and methods reusable across 
    all lambda functions in the mini-scm project.
"""

import os
import logging
from ddv.logging import log_to_stdout

RUNNING_IN_AWS = os.environ.get("AWS_EXECUTION_ENV", None) is not None

log_to_stdout(
    logging_level=logging.DEBUG,
    enable_colours=not RUNNING_IN_AWS,
    verbosity_level=0,
    verbosity_filters={
        0: [],
        1: [
            "boto3",
            "openai",
        ],
        2: [
            "botocore",
            "urllib3",
            "s3transfer",
            "boto3.resources.action",
            "httpx",
            "httpcore",
        ],
    },
    message_format="[ Mini SCM ] [ %(levelname)s ]: %(message)s",
)

# !!!
# If you change these, change the corresponding values in the terraform files
# !!!
PATH_SCANS = "scans"
PATH_OCR = "ocr"
PATH_TEXT = "text"
PATH_DATA = "data"
# !!!
# ^^^ If you change these, change the corresponding values in the terraform files
# !!!


def get_receipt_event_structure(event: dict) -> dict:
    """
    Given an ObjectCreated:Put event:
    1. Validates eventName matches the expected name
    2. Returns the s3.object.key path in the form:
        {
            "bucket": <bucket name>,
            "area": <top level directory>,
            "receipt_id": <receipt_id, second level directory>,
            "receipt_part": <receipt_part, file>
        }
    """
    first_record = event["Records"][0]

    if first_record["eventName"] != "ObjectCreated:Put":
        raise ValueError("Event is not an ObjectCreated:Put event")

    key = first_record["s3"]["object"]["key"]
    key_parts = key.split("/")
    return {
        "bucket": first_record["s3"]["bucket"]["name"],
        "area": key_parts[0],
        "receipt_id": key_parts[1],
        "receipt_part": (key_parts[2].split(".")[0] if len(key_parts) == 3 else None),
    }
