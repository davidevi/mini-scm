import requests
import logging
import os


def send_slack_message(message):
    """
    Sends a message to a slack channel
    """
    if "SLACK_WEBHOOK" not in os.environ:
        logging.debug("No slack webhook set, skipping slack message")
        return

    webhook_url = os.environ["SLACK_WEBHOOK"]
    requests.post(webhook_url, json={"text": message})
    logging.info(f"Sent slack message: {message}")
