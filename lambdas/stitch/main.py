"""
    Reads OCR results for different parts of a receipt and stitches them together

    Trigger should be:
    - Is an upload
    - Is a JSON file
    - In the "OCR" directory
    - Is the first part of a receipt
"""

import boto3
import time
import json
import logging
import sys

from receipts import get_receipt_event_structure, PATH_SCANS, PATH_OCR, PATH_TEXT
from slack import send_slack_message

S3 = boto3.client("s3")


def lambda_handler(event, context):

    try:
        info = get_receipt_event_structure(event)

        if info["receipt_part"] != "1":
            logging.info(f"Skipping stitching for part {info['receipt_part']} of {info['receipt_id']}")
            return

        send_slack_message(f"[ {info['receipt_id']} ] Stitching together OCR results")

        logging.info(f"Stitching together OCR results for {info['receipt_id']}")

        number_of_receipt_parts = get_number_of_receipt_parts(info["bucket"], info["receipt_id"])
        logging.debug(f"Number of receipt parts: {number_of_receipt_parts}")

        wait_for_all_text_extracted(info["bucket"], info["receipt_id"], number_of_receipt_parts, max_wait_time=60)

        full_text = stitch_together_receipt_parts(info["bucket"], info["receipt_id"], number_of_receipt_parts)

        save_full_text(info["bucket"], info["receipt_id"], full_text)

        send_slack_message(
            f"[ {info['receipt_id']} ] Receipt has been stitched together from {number_of_receipt_parts} parts"
        )
        return full_text

    except Exception as e:
        logging.error(f"Error stitching receipt: {e}")
        send_slack_message(f"Error stitching receipt: {e}")
        sys.exit(1)


def get_number_of_receipt_parts(bucket, receipt_id):
    response = S3.list_objects_v2(Bucket=bucket, Prefix=f"{PATH_SCANS}/{receipt_id}/")
    image_files = [obj["Key"] for obj in response.get("Contents", [])]
    return len(image_files)


def get_number_of_ocrs(bucket, receipt_id):
    response = S3.list_objects_v2(Bucket=bucket, Prefix=f"{PATH_OCR}/{receipt_id}/")
    ocr_files = [obj["Key"] for obj in response.get("Contents", [])]
    return len(ocr_files)


def wait_for_all_text_extracted(bucket, receipt_id, receipt_parts, max_wait_time):
    """
    Wait until there the receipt has all text extracted
    """
    total_wait_time = 0

    logging.info(
        f"Waiting for all text to be extracted for all {receipt_parts} parts of receipt {receipt_id} (max wait: {max_wait_time} seconds)"
    )

    while total_wait_time < max_wait_time:
        receipts_ocrs = get_number_of_ocrs(bucket, receipt_id)
        if receipt_parts == receipts_ocrs:
            logging.info("All text extracted")
            break
        time.sleep(5)
        total_wait_time += 5
        logging.info(f"Waiting 5 more seconds (current total: {total_wait_time} out of max {max_wait_time})")

    if total_wait_time >= max_wait_time:
        logging.error("Timed out waiting for all text to be extracted")
        raise TimeoutError("Timed out waiting for all text to be extracted")


def get_text_for_part(bucket: str, receipt_id: str, part_number: int) -> str:
    logging.debug(
        f"Loading text for part {part_number} of {receipt_id} from s3://{bucket}/{PATH_OCR}/{receipt_id}/{part_number}.json"
    )
    response = S3.get_object(Bucket=bucket, Key=f"{PATH_OCR}/{receipt_id}/{part_number}.json")
    raw_text = response["Body"].read().decode("utf-8")

    receipt_part_blocks = json.loads(raw_text)

    text = ""

    for block in receipt_part_blocks:
        if block["BlockType"] == "LINE":
            text += block["Text"] + "\n"

    logging.info(f"Successfully found text for part {part_number} of {receipt_id}")
    return text


def stitch_together_receipt_parts(bucket: str, receipt_id: str, number_of_parts: int) -> str:
    parts_text = []
    for part_number in range(1, number_of_parts + 1):
        parts_text.append(get_text_for_part(bucket, receipt_id, part_number))
    return "".join(parts_text)


def save_full_text(bucket, receipt_id, full_text):
    S3.put_object(
        Bucket=bucket,
        Key=f"{PATH_TEXT}/{receipt_id}/full_text.txt",
        Body=full_text.encode("utf-8"),
    )
    logging.info(f"Full text saved to s3://{bucket}/{PATH_TEXT}/{receipt_id}/full_text.txt")
