# Mini Supply Chain Manager

Serverless application for monitoring usage of household consumables and their cost, by uploading receipts. 

# Architecture 

<img src="./docs/mini-scm.png" />

# Requirements

- AWS Account and keys
- Open AI Account and key
- AWS SES Identity set up and verified
- Python (see `.python-version` for version)
- Python Poetry installed globally
- Terraform (see `.terraform-version` for version)
- AWS Identity Centre User (For accessing Grafana)

Before starting, install dependencies:
```shell
poetry install 
```

# Usage

Invoke tasks have been created for all major operations

```shell
# List all available tasks
poetry run invoke --list

# Create a function so you don't have to type
# the entire thing before every command 
pri () { 
    poetry run invoke "$@" 
}

# Run 'ocr' lambda locally with '/data/events/0_scan_uploaded_1.json' event
pri run ocr --event 0_scan_uploaded_1

# Run 'stitch' lambda locally with '/data/events/1_ocr_done.json' event
pri run stitch --event 1_ocr_done

# Run 'structure' lambda locally with '/data/events/2_stitch_done.json' event
pri run structure --event 2_stitch_done

# Deploys `dev` environment using `environments/dev.ini` as config
pri deploy dev 

# Test the environment 
# (Requires data/receipts/real/1/*.jpeg)
pri sendmail dev 1

# Destroys `dev` environment using `environments/dev.ini` as config
pri destroy dev 
```

# Environments

Use `environments/reference.ini` and `environments/reference.secrets.ini` as 
reference for creating a new environment
