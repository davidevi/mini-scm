"""

    Do NOT rename to 'email.py', as it will conflict with the built-in 'email' module

"""

from datetime import datetime
import os
import smtplib
import sys
from email.message import EmailMessage

from invoke.tasks import task

from tasks.config import FOLDERS, load_env_config


@task
def sendmail(context, env, receipt):
    """
    Given an {env} config and a {receipt} name, sends an email with the receipt images
    """
    load_env_config(env, sys.modules[__name__])

    email = create_email(
        from_address=TEST_EMAIL_USER,
        to=f"receipts-{ENVIRONMENT_NAME}@{EMAIL_DOMAIN}",
        attachements=get_attachements(receipt),
        receipt_name=receipt,
    )

    send_email(
        smtp_server=TEST_EMAIL_SMTP_SERVER, smpt_user=TEST_EMAIL_USER, smpt_password=TEST_EMAIL_PASSWORD, email=email
    )


def get_attachements(receipt_name):

    receipt_folder = os.path.join(FOLDERS["TEST_REAL_RECEIPTS"], receipt_name)

    if not os.path.exists(receipt_folder):
        raise FileNotFoundError(f"Receipt folder {receipt_folder} not found")

    return [os.path.join(receipt_folder, file) for file in os.listdir(receipt_folder)]


def create_email(from_address, to, attachements, receipt_name):
    """
    Creates an email object with the necessary fields
    """
    email = EmailMessage()
    email["From"] = from_address
    email["To"] = to
    email["Subject"] = f"Test Receipt (Receipt {receipt_name})"
    email["Date"] = datetime.now().strftime("%a, %d %b %Y %H:%M:%S %z")

    for attachment in attachements:
        with open(attachment, "rb") as file:
            email.add_attachment(file.read(), maintype="image", subtype="jpeg", filename=os.path.basename(attachment))

    return email


def send_email(smtp_server, smpt_user, smpt_password, email):
    """
    Sends an email using the given SMTP server and credentials
    """
    with smtplib.SMTP_SSL(smtp_server) as server:
        server.login(smpt_user, smpt_password)
        server.send_message(email)
