import os
import sys

from invoke.tasks import task
from importlib import import_module

from tasks.config import FOLDERS, load_env_config


@task
def deploy(context, env, dry=False):
    """
    Deploys an environment configured from "./environments/{env}.ini"
    """
    load_env_config(env, sys.modules[__name__])

    create_dependencies_layer(context)

    create_env(context, env, dry)


@task
def destroy(context, env):
    """
    Destroys an environment configured from "./environments/{env}.ini"
    """
    load_env_config(env, sys.modules[__name__])

    destroy_env(context, env)


def create_dependencies_layer(context):
    """
    Ensures that all the dependencies are installed in the dependencies layer
    (with the correct python version and platform)
    """

    MINI_SCM_DEPS_PATH = os.path.join(FOLDERS["LAMBDAS"], "mini_scm_dependencies")

    if os.path.exists(f"{MINI_SCM_DEPS_PATH}/requirements.txt"):
        context.run(
            f"cd {MINI_SCM_DEPS_PATH} && \
            pip freeze > new_requirements.txt"
        )
        with open(f"{MINI_SCM_DEPS_PATH}/requirements.txt") as f:
            old_requirements = f.read()
        with open(f"{MINI_SCM_DEPS_PATH}/new_requirements.txt") as f:
            new_requirements = f.read()
        if old_requirements == new_requirements:
            os.remove(f"{MINI_SCM_DEPS_PATH}/new_requirements.txt")
            return

    context.run(
        f"""cd {MINI_SCM_DEPS_PATH} && \
            pip freeze > requirements.txt && \
            pip install -r requirements.txt \
            --only-binary=:all: \
            --upgrade \
            --platform manylinux2014_x86_64 \
            --implementation cp \
            --python-version 3.12 \
            -t python
        """
    )


def create_env(context, env, dry=False):

    os.chdir("terraform")

    context.run(
        f"""
        terraform init \
        -migrate-state \
        -backend-config="bucket={TERRAFORM_STATE_BUCKET}" \
        -backend-config="region={AWS_REGION}" 
    """
    )
    # -backend-config="dynamodb_table={TERRAFORM_STATE_LOCK_TABLE}" \

    context.run(f"terraform workspace select -or-create {get_workspace(env)}")
    context.run(f"terraform plan -out=tfplan")
    # context.run("terraform graph -type=plan | dot -Tpng > graph.png")

    if not dry:
        context.run(f"terraform apply tfplan", env=os.environ)


def destroy_env(context, env):
    os.chdir("terraform")
    context.run(f"terraform workspace select {get_workspace(env)}")
    context.run(f"terraform destroy")


def get_workspace(env):
    return f"miniscm-{env}"
