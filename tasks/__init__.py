from invoke import Collection, task
from running import run
from deployment import deploy, destroy
from email_testing import sendmail

tasks = Collection()

tasks.add_task(run)
tasks.add_task(deploy)
tasks.add_task(destroy)
tasks.add_task(sendmail)

namespace = tasks
