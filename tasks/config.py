import os
import sys

from ddv.settings import read, export

FOLDERS = {
    "EVENT_DATA": "data/events",
    "TEST_REAL_RECEIPTS": "data/receipts/real",
    "ENVIRONMENTS": "./environments",
    "LAMBDAS": "lambdas",
    "TERRAFORM_LAMBDAS": "terraform/lambdas",
    "OUT": "out",
}


def load_env_config(env, target_module):

    standard_config = f"{FOLDERS['ENVIRONMENTS']}/{env}.ini"
    secrets = f"{FOLDERS['ENVIRONMENTS']}/{env}.secrets.ini"

    for config in [standard_config, secrets]:
        if not os.path.exists(config):
            raise FileNotFoundError(f"Config file {config} not found")
        read(config)
        export(target_module=target_module)
        export(export_env=True, prefix="TF_VAR")

    # print(dir(sys.modules[__name__]))
    # print(os.environ)
