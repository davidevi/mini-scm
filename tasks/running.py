import os
import json
import sys
import logging


from invoke.tasks import task
from importlib import import_module

from tasks.config import FOLDERS, load_env_config


@task
def run(context, name, event=None):
    """
    Run the {name} Lambda function locally; Pass {event} to specify the event data file (without the .json extension)
    """
    load_env_config("dev", sys.modules[__name__])
    event_data = load_event_from_file(event)
    sys.path.append(".")
    sys.path.append("lambdas/mini_scm_common/python")
    module = import_module(f"lambdas.{name}.main")
    os.chdir(f"lambdas/{name}")

    result = module.lambda_handler(event_data, None)

    print("----------------")
    print(f"Result:\n{result}")

    OUT_DIR = f"../../{FOLDERS['OUT']}"

    if not os.path.exists(OUT_DIR):
        os.makedirs(OUT_DIR)

    with open(f"{OUT_DIR}/{name}-{event}.txt", "w") as f:
        f.write(str(result))


def load_event_from_file(event_file):
    """
    Load event data from a file
    """
    if event_file is None:
        return {}

    event_file = os.path.join(os.getcwd(), FOLDERS["EVENT_DATA"], f"{event_file}.json")
    if not os.path.isfile(event_file):
        print(f"Event file {event_file} not found")
        sys.exit(1)

    with open(event_file, "r") as f:
        return json.load(f)
